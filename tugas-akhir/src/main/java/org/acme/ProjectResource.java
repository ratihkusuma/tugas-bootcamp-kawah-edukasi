package org.acme;

import org.acme.entities.User;
import org.acme.entities.BMI;
import org.acme.entities.WorkOut;;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProjectResource {


    @GET
    @Path("all")
    public List<User> getUsers(){
        return User.listAll();
    }

    @GET
    @Path("id")
    public User getUsersById(Long id){
        return User.findById(id);
    }

    @POST
    @Transactional
    public User addUser(User user){
        user.persist();
        return user;
    }

    @PUT
    @Path("id")
    @Transactional
    public User updateUser(@PathParam("id") Long id, User newUser){
        User oldUser= User.findById(id);
        oldUser.username = newUser.username;
        oldUser.email = newUser.email;
        oldUser.birthDate = newUser.birthDate;
        oldUser.sex = newUser.sex;
        return oldUser;
    }

    @DELETE
    @Path("id")
    @Transactional
    public Boolean deleteUser(@PathParam("id") Long id){
        return User.deleteById(id);
    }


}