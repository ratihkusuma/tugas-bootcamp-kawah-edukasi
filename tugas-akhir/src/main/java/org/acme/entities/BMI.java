package org.acme.entities;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="bmis")
public class BMI extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    public Float userIndex;
    public String category;
    public Float beratIdeal;


    @JsonGetter
    public Long getId(){
        return this.id;
    }

    @JsonSetter
    @JsonIgnore
    public void setId(Long id){
        this.id=id;
    }
}
