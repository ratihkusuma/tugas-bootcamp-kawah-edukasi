package org.acme.entities;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;

@Entity
@Table(name="workouts")
public class WorkOut extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    public String workOutType;
    public String workOutName;
    public Integer repetition;

    @JsonGetter
    public Long getId(){
        return this.id;
    }

    @JsonSetter
    @JsonIgnore
    public void setId(Long id){
        this.id=id;
    }

}
