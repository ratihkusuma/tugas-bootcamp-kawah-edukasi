import java.util.Scanner;

public class TugasSatu {
    public static void main(String[] args) {
        //Tugas pertama: membuat program sederhana
        Scanner input=new Scanner(System.in); //membuat input Scanner

        System.out.print("TUGAS PERTAMA BOOTCAMP KAWAH EDUKASI\n" +
                "Membuat Program Sederhana\n\n" +
                "MENU:\n1 Identitas\n2 Kalkulator\n3 Perbandingan" +
                "\n\nSilakan memilih menu dengan mengetikkan ANGKA MENU: ");
        int pilihanMenu = input.nextInt();
        //input pilihan menu yang diinginkan oleh user

        if(pilihanMenu==1){
            //Jika input=1 (menu identitas) maka akan muncul Nama, alasan, dan ekspektasi
            System.out.println("MENU IDENTITAS");
            System.out.println("Nama: Ratih Kusuma Wijayanti");
            System.out.println("Alasan menjadi BE Developer: Karena ngoding itu seru hehe.. Dunia pemrograman juga " +
                    "selalu ada perkembangan dan menjadi pelopor dari perubahan yang ada.");
            System.out.println("Ekspektasi mengikuti bootcamp KE: Selama ini lumayan bingung untuk memulai belajar " +
                    "dari mana dan merasa tidak yakin apakah pemahaman yang saya miliki sudah cukup untuk terjun " +
                    "di dunia kerja. Jadi dengan adanya bootcamp ini, saya berharap kebingungan saya bisa terjawab" +
                    " hehe :)" +
                    "\nSetelah selesai dari bootcamp ini saya berharap bisa mencoba bekerja sebagai BE Developer.");

        } else if (pilihanMenu==2) {
            //Jika input=2 (menu kalkulator) maka akan muncul menu +, -, *, /, mod

            System.out.println("MENU KALKULATOR:\n1 Penjumlahan\n2 Pengurangan\n3 Perkalian\n4 Pembagian\n5 Modulo");
            System.out.println("Menu kalkulator yang diinginkan: ");
            int pilihanKalkulator = input.nextInt();
            //input menu kalkulator oleh user

            System.out.println("Masukkan angka pertama: ");
            float angkaPertama = input.nextFloat();
            //input bilangan pertama oleh user

            System.out.println("Masukkan angka kedua: ");
            float angkaKedua = input.nextFloat();
            //input bilangan kedua oleh user

            String namaKalkulator="";
            float hasil=0;
            switch (pilihanKalkulator){
                case 1:
                //Jika user memilih menu 1 (penjumlahan), output adalah hasil penjumlahan
                    hasil=angkaPertama+angkaKedua;
                    namaKalkulator="penjumlahan";
                    break;
                case 2:
                //Jika user memilih menu 2 (pengurangan), output adalah hasil pengurangan
                    hasil=angkaPertama-angkaKedua;
                    namaKalkulator="pengurangan";
                    break;
                case 3:
                    //Jika user memilih menu 3 (Perkalian), output adalah hasil perkalian
                    hasil=angkaPertama*angkaKedua;
                    namaKalkulator="perkalian";
                    break;
                case 4:
                    //Jika user memilih menu 4 (pembagian), output adalah hasil pembagian
                    hasil=angkaPertama/angkaKedua;
                    namaKalkulator="pembagian";
                    break;
                case 5:
                    //Jika user memilih menu 5 (modulo), output adalah hasil modulo
                    hasil=angkaPertama%angkaKedua;
                    namaKalkulator="modulo";
                    break;
            }
            System.out.println("Hasil operasi "+namaKalkulator+" adalah "+hasil);
            //print hasil operasi

        } else if (pilihanMenu==3) {
            //Jika input=3 maka akan dialihkan ke menu perbandingan
            System.out.println("MENU PERBANDINGAN");

            System.out.println("Masukkan angka pertama: ");
            float angkaPembanding1 = input.nextFloat();
            //input bilangan pertama oleh user

            System.out.println("Masukkan angka kedua: ");
            float angkaPembanding2 = input.nextFloat();
            //input bilangan kedua oleh user

            if(angkaPembanding1>angkaPembanding2){
                System.out.println(angkaPembanding1+" lebih besar dari "+angkaPembanding2);
                //output print jika angka pertama lebih besar dari angka kedua

            } else if(angkaPembanding1<angkaPembanding2){
                System.out.println(angkaPembanding1+" lebih kecil dari "+angkaPembanding2);
                //output print jika angka pertama lebih kecil dari angka kedua

            } else {
                System.out.println("Kedua bilangan sama");
                //output print jika kedua bilangan sama
            }

        } else {
            //jika user salah memasukkan angka, maka akan muncul peringatan dan keluar dari program
            System.out.println("INPUT SALAH");
        }
    }
}
