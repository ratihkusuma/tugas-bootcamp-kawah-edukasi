import java.util.Scanner;

public class TugasDua {
    public static void main(String[] args) {
        //Tugas kedua: membuat program menggunakan beberapa method
        Scanner input=new Scanner(System.in); //membuat input Scanner

        System.out.print("TUGAS KEDUA BOOTCAMP KAWAH EDUKASI\n\n" +
                "MENU:\n1 Nilai Mutlak\n2 Nilai Terbesar\n3 Perpangkatan" +
                "\n\nSilakan memilih menu dengan mengetikkan ANGKA MENU: ");
        int pilihanMenu = input.nextInt();
        //input pilihan menu yang diinginkan oleh user

        if (pilihanMenu==1){
            //Menu nilai mutlak hanya menerima 1 parameter dengan tipe data angka(float)
            System.out.println("MENU NILAI MUTLAK\n");
            
            System.out.println("Masukkan angka: ");

            int valueA = input.nextInt();
            //input angka yang diinginkan oleh user
            System.out.println("\nNilai mutlak = "+nilaiMutlak(valueA));
        } else if (pilihanMenu==2){
            //Menu 2 akan memberikan hasil nilai terbesar
            System.out.println("MENU NILAI TERBESAR\n");
            
            System.out.println("Masukkan banyak bilangan: ");

            int lengthArray = input.nextInt();
            //input banyaknya angka yang akan dibandingkan oleh user
            if (lengthArray==0) { 
                //program berhenti apabila banyaknya data = 0
                return;
            }
            
            int myArray[]= new int [lengthArray];
            //deklarasi array kosong

            for (int i=0; i<lengthArray; i++){
            //input angka yang akan dibandingkan
                System.out.println("Nilai ke-"+(i+1)+" = ");
                int valueA = input.nextInt();
                myArray[i]=valueA;
            }
            
            System.out.println("\nAngka terbesarnya adalah "+biggestVal(myArray));
            
        } else if (pilihanMenu==3){
            //Jika input=3 maka akan dialihkan ke menu perbandingan
            System.out.println("MENU PERPANGKATAN");

            System.out.println("Masukkan angka basis: ");

            int basis = input.nextInt();
            // angka basis untuk perpangkatan

            System.out.println("Masukkan pangkat: ");

            int eksponen = input.nextInt();
            //input bilangan eksponensial

            System.out.println("Hasil perpangkatan= "+pangkat(basis,eksponen));
            //mencetak hasil perpangkatan
        }
    }
    public static int nilaiMutlak(int firstVal){
        //method untuk menentukan nilai mutlak
        if (firstVal<0){
            return -firstVal;
        } else {
            return firstVal;
        }
    }

    public static int biggestVal(int arrayValue[]){
        //method untuk mencari angka terbesar
        int temp=arrayValue[0];
        //temp berguna sebagai temporary slot untuk bilangan yang dibandingkan
		for (int j=1; j<arrayValue.length; j++) {
			if(temp<arrayValue[j]){
                temp=arrayValue[j];
                //jika angka pembanding(temp) lebih kecil dibanding angka pada komponen array ke-j, maka angka pembanding(temp) akan digantikan oleh array ke-j
            }
		}
        return temp;
    }

    public static float pangkat(float basis, int eksponen){
        //method untuk perpangkatan, tidak berlaku untuk pangkat pecahan(bentuk akar)
        float hasilPow;
        if (eksponen<0){
            //untuk pangkat negatif

            hasilPow=1/basis;
            //hasilPow mendefinisikan variabel untuk menyimpan hasil iterasi perkalian
            //pangkat negatif (a^-n) berarti 1/a^n 
            float newBasis=1/basis;
            int j=-1*eksponen;
            for (int i=1; i<j; i++){
                hasilPow=hasilPow*newBasis;
            }
            
        } else if(eksponen>0) {
            //untuk pangkat positif
            hasilPow=basis;
            for (int i=1; i<eksponen; i++){
                hasilPow=hasilPow*basis;
            }

        } else {
        //jika pangkat 0, maka hasilnya adalah 1
            hasilPow=1;
        }
        return hasilPow;
    }
}
