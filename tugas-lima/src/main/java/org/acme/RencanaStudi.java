package org.acme;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Min;
import javax.validation.constraints.Max;
public class RencanaStudi {

    @NotBlank(message="Nama wajib diisi")
    @Length(min=2, max=33, message="Panjang nama 2-30 karakter")
    public String namaMahasiswa;

    @Min(value=3, message="Peminatan baru bisa di semester 3")
    @Max(value=14, message="Terlalu lama jadi beban keluarga")
    public Integer semester;

    @NotBlank(message="Pilihan peminatan pertama wajib diisi")
    public String pilihanPeminatanPertama;

    @NotBlank(message="Pilihan peminatan kedua wajib diisi")
    public String pilihanPeminatanKedua;

    public RencanaStudi(){

    }

    public RencanaStudi(String nama, Integer semester, String pilihanSatu, String pilihanDua){
        this.namaMahasiswa=nama;
        this.semester=semester;
        this.pilihanPeminatanPertama=pilihanSatu;
        this.pilihanPeminatanKedua=pilihanDua;
    }


}