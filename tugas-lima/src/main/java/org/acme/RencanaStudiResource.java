package org.acme;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Path("/krs")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RencanaStudiResource {

    public Map<Integer, RencanaStudi> kartuRencanaStudi = new HashMap<>();
    public Integer nim=1;


    @Inject
    Validator validator;
    public RencanaStudiResource(){
        RencanaStudi rencana1 = new RencanaStudi("Mahasiswa 1",3,"Aljabar","Analisis");
        RencanaStudi rencana2 = new RencanaStudi("Mahasiswa 2",5,"Statistika","Matematika Terapan");
        kartuRencanaStudi.put(nim++, rencana1);
        kartuRencanaStudi.put(nim++, rencana2);
    }

    public static class Result {

        Result(String message) {
            this.success = true;
            this.message = message;
        }

        Result(Set<? extends ConstraintViolation<?>> violations) {
            this.success = false;
            this.message = violations.stream()
                    .map(cv -> cv.getMessage())
                    .collect(Collectors.joining(", "));
        }

        private String message;
        private boolean success;

        public String getMessage() {
            return message;
        }

        public boolean isSuccess() {
            return success;
        }

    }
    @GET
    public Map<Integer, RencanaStudi> getKartuRencanaStudi(){
        return kartuRencanaStudi;
    }

    @GET
    @Path("{nim}")
    public RencanaStudi getRencanaStudiByNIM( @PathParam("nim") Integer nim){
        return kartuRencanaStudi.get(nim);
    }

    @POST
    public Result addRencanaStudi(RencanaStudi rencanaStudi){
        Set<ConstraintViolation<RencanaStudi>> violations = validator.validate(rencanaStudi);
        if (violations.isEmpty()){
            kartuRencanaStudi.put(nim++,rencanaStudi);
            return new Result("Rencana Studi baru telah ditambahkan");
        } else {
            return new Result(violations);
        }
    }
    @PUT
    @Path("{nim}")
    public Object updateRencanaStudi(@PathParam("nim") Integer nim,@Valid RencanaStudi newRencanaStudi){
        Map<String, RencanaStudi> pembaruan = new HashMap<>();
        pembaruan.put("KRS Lama", kartuRencanaStudi.get(nim));
        kartuRencanaStudi.replace(nim, newRencanaStudi);
        pembaruan.put("pembaruanRencanaStudi", kartuRencanaStudi.get(nim));
        return pembaruan;
    }
    @DELETE
    @Path("{nim}")
    //menghapus data object dengan nim tertentu
    public RencanaStudi deleteRencanaStudi(@PathParam("nim") Integer nim){
        //menyimpan data yang akan dihapus pada variabel sementara "temp"
        RencanaStudi temp = kartuRencanaStudi.get(nim);

        //menghapus data dengan nim tertentu
        kartuRencanaStudi.remove(nim);

        //mengembalikan data yang telah dihapus
        return temp;
    }

    @DELETE
    @Path("emptyData")
    public String emptyRencanaStudi(){
        kartuRencanaStudi.clear();
        nim=1;
        return "Semua data telah terhapus";
    }


}